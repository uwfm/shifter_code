# shifter_code

This is the code to run on the arduino attached to the SCU, which controls our pneumatic shifter

If you are looking for a version of code to run on the car, you will need to download the "2019_SCU_Sketch" folder (the entire folder). Open the 2019_SCU_SKetch.ino file inside the folder with the arduino IDE, and use the button in the top left to program the board.