git b/* University of Waterloo Formula Motorsports
    2019 SCU Tuning
    NICK Pellegrino - npellegr@uwaterloo.ca
    SAM  Swift      - seswift@uwaterloo.ca
    Release v1.0
    -First release with injection cut
    -Re-implemented shifting into neutral
*/

//Includes for CAN Libraries
#include <Canbus.h>
#include <defaults.h>
#include <global.h>
#include <mcp2515.h>
#include <mcp2515_defs.h>

// Define analog arduino pins
int downButton = A0;
int upButton = A1;
int clutchButton = A4;           //Pin 23, terminal 3 
int auxIn[] = {A2,A3,A4,A5};
int PDUFuseTrippedSignal = A2;
bool fuseTrippedMessageSent = 0;

// Define digital arduino pins
int injectorCut = 4;
int clutchSolenoid = 5;
int downSolenoid = 6;
int upSolenoid = 7; 
int clutchLight = A5; // Writing the pin LOW turns on the light and HIGH turns off the light


// ADC steps for paddle to be considered high or low
int analogHigh = 900;
int analogLow = 800;

// define time delays (ms)
int upShiftDelay = 75;// shifter engaged before releasing
int clutchDelay = 150; // disengage clutch before down shifting
int downShiftDelay = 200; // shifter engaged before releasing
int downEngageDelay = 25; // leave clutch engaged before releasing down shifter
int neutralDelay = 13; // to put the car in neutral from first

// State Variables
bool clutchActivated = false;

void setup() {

  Serial.begin(9600);

  // set arduino pins
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
  pinMode(clutchButton, INPUT);
  for(int i = 0; i < 1; i++) {
    pinMode(auxIn[i], INPUT);
  }
  pinMode(upSolenoid, OUTPUT);
  pinMode(downSolenoid, OUTPUT);
  pinMode(clutchSolenoid, OUTPUT);
  pinMode(injectorCut, OUTPUT); 
  pinMode(clutchLight, OUTPUT);


  // initialize outputs
  digitalWrite(upSolenoid, LOW);
  digitalWrite(downSolenoid, LOW);
  digitalWrite(clutchSolenoid, LOW);
  digitalWrite(injectorCut, HIGH);
  digitalWrite(clutchLight, HIGH);


  if(Canbus.init(0))  //Initialise MCP2515 CAN controller at the specified speed
    Serial.println("CAN Init ok");
  else
    Serial.println("Can't init CAN");

  delay(1000);
}

void upShift() {
  digitalWrite(injectorCut, LOW);
  sendInjCutState(1);
  digitalWrite(upSolenoid, HIGH);
  
  delay(upShiftDelay);
  
  digitalWrite(upSolenoid, LOW);
  digitalWrite(injectorCut, HIGH);
  sendInjCutState(0); 
}

void downShift() {
  digitalWrite(clutchSolenoid, HIGH);
  delay(clutchDelay);

  digitalWrite(downSolenoid, HIGH);
  delay(downShiftDelay);
  digitalWrite(downSolenoid, LOW);

  delay(downEngageDelay);
  digitalWrite(clutchSolenoid, LOW);
}

void neutral(){
  digitalWrite(upSolenoid, HIGH);
  delay(neutralDelay);
  digitalWrite(upSolenoid, LOW);
}

void toggleClutch()
{
  if(clutchActivated)
  {
    digitalWrite(clutchSolenoid, LOW);                
    digitalWrite(clutchLight, HIGH);         
  }
  else
  {
    digitalWrite(clutchSolenoid, HIGH);
    digitalWrite(clutchLight, LOW);
  }
  clutchActivated = !clutchActivated;
}

void loop() {
  if (analogRead(upButton) > analogHigh) {
    upShift();
    while(analogRead(upButton) > analogLow);
  }
  if (analogRead(downButton) > analogHigh) {
    downShift();
    while(analogRead(downButton) > analogLow){
      if (analogRead(upButton) > analogHigh){
        neutral();
        while(analogRead(upButton) > analogLow){};
      }
    }
  }
  if (digitalRead(PDUFuseTrippedSignal) == HIGH) {
    if(!fuseTrippedMessageSent){
      sendFuseTripped();
      fuseTrippedMessageSent = 1;
    }
  }
  if (analogRead(clutchButton) > analogHigh) {
    toggleClutch();
    delay(500);
    while(analogRead(clutchButton) > analogLow);
  }
  delay(1);
}

void sendInjCutState(boolean cut)
{
  tCAN message;

  message.id = 0x100; //formatted in HEX
  message.header.rtr = 0;
  message.header.length = 1; //formatted in DEC
  message.data[0] = cut;

  mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
  mcp2515_send_message(&message);
}

void sendFuseTripped()
{
  tCAN message;

  message.id = 0x101; //formatted in HEX
  message.header.rtr = 0;
  message.header.length = 1; //formatted in DEC
  message.data[0] = 1;

  mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
  mcp2515_send_message(&message);
}
// EOF
